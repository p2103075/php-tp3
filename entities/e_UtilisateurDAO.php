<?php

require_once(PATH_MODELS . 'DAO.php');
require_once(PATH_ENTITY . 'user.php');


class UtilisateurDAO extends DAO
{
    public function getUser(string $str)
    {
        $res = $this->queryRow("Select * FROM Utilisateur WHERE login = ?", array(($str)));
        if ($res != false) return new Utilisateur($res["login"], $res["mot"], $res["nbr"]);
    }
}
