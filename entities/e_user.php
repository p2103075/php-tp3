<?php
class Utilisateur
{
    private $_login;
    private $_mot;
    private $_nbr;

    public function __construct(string $log, string $mot, int $nb)
    {
        $this->_login = $log;
        $this->_mot = $mot;
        $this->_nbr = $nb;
    }

    public function getLogin()
    {
        return $this->_login;
    }

    public function getMot()
    {
        return $this->_mot;
    }

    public function getNbRepet()
    {
        return $this->_nbr;
    }
}
