<?php
/*
 * DS PHP
 * Controller page accueil
 *
 * Copyright 2017, Eric Dufour
 * http://techfacile.fr
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */



//appel de la vue

const TITRE = TITRE_PAGE_ACCUEIL;

require_once(PATH_VIEWS . $page . '.php');
