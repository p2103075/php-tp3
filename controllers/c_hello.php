<?php

if (isset($_GET['login']) && !empty($_GET['login'])) {
    require_once(PATH_ENTITY.'UtilisateurDAO.php');
    $userdao = new UtilisateurDAO(DEBUG);
    $user = $userdao->getUser($_GET['login']);
    echo $userdao->getErreur();
    if ($user != false){
    
        require_once(PATH_VIEWS.$page.'.php');
    }
    else{
        
        $alert = choixAlert('login');   
        require_once(PATH_VIEWS.'accueil.php');
    }
    
}
else{
    require_once(PATH_VIEWS.'accueil.php');
}
