<?php
//isoler ici dans des constantes les textes affichés sur le site
define('LOGO', 'Logo de la compagnie'); // Affiché si image non trouvée
define('TITRE', 'un supert titre');
define('MENU_ACCUEIL','Accueil');
define('TITRE_PAGE_ACCUEIL','Accueil');
define('TITRE_PAGE_HELLO','Résultat');
define('ERREUR_CONNECT_BDD', 'Erreur de connexion à la base de donné');
define('ERREUR_INSCRIPTION', 'Erreur lors de l\'inscription');
define('ERREUR_QUERY_BDD', 'Erreur de requette à la base de donné');
define('TEXTE_PAGE_404','Oops, la page demandée n\'existe pas !');
define('MESSAGE_ERREUR',"Une erreur s'est produite");
define('TITRE_PAGE_ACCUEIL_TOUS', 'titre');
